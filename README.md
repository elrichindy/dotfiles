# My Dotfiles
A collections of dotfiles and scripts to install and set up computers

## Scripts
### install_i3_ranger_urxvt
* compton
* feh
* i3
* urxvt

### install_vim_vundle_ycm_colour
* vim latest (8+ as of writing)
* vundle package manager
* YCM (You Complete Me) with node and c/c++ support
* luna-term theme (because i like it, mostly)

### install_fish_omf_themes
* fish shell 2+
* omf (Oh My Fish) fish package manager
* sushi theme

### install_tools
* neofetch

## Configurations
All configurations where appropriate for the above installs plus:

* A grub configuration for changing display size on hyper-v systems (which I used during testing the configs)
** After updated run
'''sudo update-grub
* A ligthdm config to remove previous logins to add a more minimalistic log in (WIP) hope to remove the window manager and replace with a clean login prompt if I can find one.

## Notes
Use scripts at own peril, best to use as a guide see README.md in script folder for more info.
Also install required packages to build and install the tools including but not limited to:
* cmake
* git
* curl
* wget
 
### Chrome
Required the following when installing chrome, note this is not in the install list just a note.
sudo apt install libappindicator1

