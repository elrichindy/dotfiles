#!/bin/bash

# Microsoft LifeCam
# options
#Brightness
#Contrast
#Saturation
#White Balance Temperature, Auto
#Power Line Frequency
#White Balance Temperature
#Sharpness
#Backlight Compensation
#Exposure, Auto
#Exposure (Absolute)
#Pan (Absolute)
#Tilt (Absolute)
#Focus (absolute)
#Focus, Auto
#Zoom, Absolute

uvcdynctrl -s 'Exposure, Auto' 3 
uvcdynctrl -s 'Brightness' 100
uvcdynctrl -s 'Contrast' 5
uvcdynctrl -s 'Saturation' 103
uvcdynctrl -s 'Sharpness' 25 
uvcdynctrl -s 'White Balance Temperature, Auto' 1
uvcdynctrl -s 'Backlight Compensation' 0
uvcdynctrl -s 'Power Line Frequency' 1
uvcdynctrl -s 'Focus, Auto' 1
