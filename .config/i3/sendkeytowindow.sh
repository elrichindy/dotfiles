#!/bin/bash

window=$1
key=$2

xdotool key --window $(xdotool search --classname $window) $key
