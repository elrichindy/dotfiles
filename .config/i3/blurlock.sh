#!/bin/bash

# Temp Capture location
TEMP_CAP="/tmp/lock_screen.png"

# Pixelate/blur
#convert /tmp/lock_screen.png -scale 10% -scale 1000% /tmp/lock_screen.png 
ffmpeg -loglevel quiet -f x11grab -video_size 3840x1080 -i $DISPLAY -i ~/.config/i3/lock/rick_morty_lock.png -y -filter_complex "boxblur=5:1,overlay=(main_w-overlay_w-10):(main_h-overlay_h-10)" -vframes 1 $TEMP_CAP

# Lock it 
i3lock -i $TEMP_CAP -e
