# set local user bin path
set PATH ~/bin $PATH 
set PATH ~/.local/bin $PATH 
source ~/.cargo/env
# enable vi mode
#set -g fish_key_bindings fish_vi_key_bindings
#set editor
set -gx EDITOR /usr/bin/vim
# use wall to set colours
wal -r &
