set PATH ~/bin/anaconda/bin $PATH 

if which conda > /dev/null
    source (conda info --root)/etc/fish/conf.d/conda.fish
end
