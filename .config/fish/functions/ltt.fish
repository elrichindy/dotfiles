function ltt --description "List contents of directory, including hidden files in directory using long format and in order of last modified first and can supplyt a time"
    #set defaults
    set type f
    set filter "*.*"
    set mtime -1
    set depth 1
    set path .
    set pathonly 0
    #get options
    getopts $argv | while read -l key option
        #echo "$key=$option"
        switch $key
            case _
                set path $option
            case f
                set type f
            case d
                set type d
            case md maxdepth
                set depth $option
            case t
                set mtime $option
            case e
                set filter $option
            case s
                set pathonly 1
            case h help
                __ltt_help
                return 0
        end
    end

    set files (find $path -maxdepth $depth -type $type -mtime $mtime -iname "$filter")[2..-1]

    if count $files > /dev/null
        if [ $pathonly -eq 0 ]
            ls -lAht $files
        else
            ls -A $files
        end
    end
end

function __ltt_help
    echo "Usage: ltt [path/to/file] [OPTIONS]"
    echo "[path/to/file] defaults to current directory"
    echo "OPTIONS:"
    echo " -f                search for files"
    echo " -d                search for directories"
    echo " -e                name filter to apply"
    echo " -s --simple       (simple) only show paths"
    echo " --md --maxdepth   depth of driectories to search"
    echo " -t [value]        mtime in find see man find for mtime(default -1)"
    echo " -h --help         show this help"
end
