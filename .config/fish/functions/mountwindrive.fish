function mountwindrive
    sudo mkdir -p /mnt/$argv[1]
    sudo chown -R $USER:$USER /mnt/$argv[1]
    sudo mount -t drvfs $argv[1]: /mnt/$argv[1]
end
