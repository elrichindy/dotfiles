function dssh
    set address $argv[1]
    set addresslist (string split -- @ $address)
    set user $addresslist[1]
    set ipaddress $addresslist[2]
    set termlocation (find /usr/share/terminfo -name $TERM)
    set termloc (string split -- / $termlocation) 
    set termfile "$termloc[-2]/$termloc[-1]"

    if ssh-keygen -F $ipaddress >/dev/null
       ssh-keygen -f "$HOME/.ssh/known_hosts" -R "$ipaddress"
    end

    if not ssh-keygen -F $ipaddress >/dev/null
        echo "Copying Keys"
        ssh-copy-id $address
    end
    
    if echo $ipaddress | grep 'fe80' >/dev/null
      set ipaddress "[$ipaddress]"
      set scpipv6 "-6"
    else
      set scpipv6 ""
    end

    if not ssh $address stat "~/.terminfo/$termfile" > /dev/null ^&1
        echo "Copying Terminfo"
        ssh $address "mkdir -p ~/.terminfo/$termloc[-2]"
        scp $scpipv6 $termlocation $user@$ipaddress:.terminfo/$termloc[-2]/
    end
    ssh $address
end
