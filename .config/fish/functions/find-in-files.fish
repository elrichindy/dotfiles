function find-in-files
  set argcount (count $argv)
  if [ $argcount -lt 2 ]
      echo "Not enough arguments supplied"
      __find_in_files_help
  else
      set path $argv[1]
      set pattern $argv[2]

      echo "searching path $path for $pattern"

      if [ $argcount -gt 2 ]
          set options $argv[3..-1]
          grep -Rna $options $path -e $pattern
      else
          grep -Rna $path -e $pattern
      end
  end
end
  

function __find_in_files_help
    echo "Usage: find-in-files path/to/search pattern [OPTIONS]"
    echo "Default options always include -Rn"
    echo "    OPTIONS"\n"        Standard grep search arguments"
end

