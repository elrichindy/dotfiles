#
# These are very common and useful
#
function lt --description "List contents of directory, including hidden files in directory using long format and in order of last modified first"
    ls -laht $argv
end

