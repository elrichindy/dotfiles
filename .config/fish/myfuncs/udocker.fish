# Some functions for working with docker containers

# Formated output of docker ps
function docker-pretty-print
    docker ps -a --format "table {{.ID}}\t{{.Names}}\t{{.Ports}}\t{{.Status}}\t{{.Networks}}\t{{.Image}}\t{{.Mounts}}" $argv
end

# nice way to remove docker none images
function docker-remove-none-images
   set images (docker images -f "dangling=true" -q)
   if count $images > /dev/null
     echo "Removing: $images"
     docker rmi -f $images
   else
     echo "No images to remove"
   end
end

# Clean up any stopped containers
function docker-remove-stopped-containers
   set containers (docker ps -aq)
   echo "Removing: $containers"
   docker rm $containers
end

# clear the logs of docker containers (they can get big)
function docker-clear-logs
    switch $argv
        case "-h*" or "*--help*"
            echo "docker-clear-logs [container name|container id]"
            return 0
    end
    set running (docker inspect -f '{{.State.Running}}' $argv ^ /dev/null)
    if [ ! $running ]
        echo "No container with name \"$argv\""
        return 1
    end

    if [ $running = 'true' ]
        echo "Please Stop Container First"
        return 1
    end
 
    set logfile (docker inspect $argv | grep -G '"LogPath": "*"' | sed -e 's/.*"LogPath": "//g' | sed -e 's/",//g')
    echo "Clearing Logfile $logfile"
    sudo rm $logfile ^ /dev/null
    if [ $status -ne 0 ]
        echo "Log file may be already removed"
    else
       echo "Successfully removed log file"
    end
end
