#!/bin/bash
set -e
if ! [ $(id -u) = 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
CUSER=${SUDO_USER}

# copy vim configs and colors
# first backup .vimrc
if [ -f ~/.vimrc ];then
	echo "copying .vimrc to .vimrc.bu"
	su $CUSER -c "cp ~/.vimrc ~/.vimrc.bu"
else
	echo "no .vimrc file to copy"
fi

add-apt-repository -u -y ppa:jonathonf/vim
sudo apt install -y vim
echo "make vim default"
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 100

su $CUSER -c "cp $WDIR/.vimrc ~/.vimrc"
# no .vim directory yet so need to create one.
su $CUSER -c "mkdir -p ~/.vim/colors"
su $CUSER -c "cp $WDIR/.vim/colors/* ~/.vim/colors/"

# build requirements for vim plugins and ycm
DEBIAN_FRONTEND=noninteractive apt update && apt install -y --no-install-recommends \
    build-essential cmake python-dev python3-dev curl git

# node is required for --tern-completer option in ycm
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends nodejs 

# install vim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# .vimrc copied so just run the install
su $CUSER -c "vim +PlugInstall +qall"
# build YCM
su $CUSER -c "cd ~/.vim/plugged/YouCompleteMe && ./install.py --clang-completer --tern-completer"
 # clean up
echo "Cleaning up vim install"
su $CUSER -c "rm -rf ~/.vim/plugged/YouCompleteMe/third_party/ycmd/clang_archives/*"

echo "finished installing vim, ycm and plugins"

