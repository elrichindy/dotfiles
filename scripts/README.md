# Scripts to install and setup packages
This is a collection of scripts to install the various related tools and commands used in my config

## Install Order
The recomended install order is:
1. i3 ranger urxvt
1. vim vundle ycm and theme/colours
1. fish shell with omf and themes
1. Additional tools you may want

## USE WITH CAUTION
These scripts are specific to ubuntu and as such may not work correctly or at all.
Use as a guide for your system and follwing the script flow using your os specific options

