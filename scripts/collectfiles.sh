#!/bin/bash

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

# fish 
if [ -d ~/.config/fish ]; then
    echo "Collecting fish configs"
    #rm all current fish files
    rm -rf $WDIR/.config/fish
    mkdir -p $WDIR/.config/fish/conf.d
    cp ~/.config/fish/conf.d/u* $WDIR/.config/fish/conf.d
    cp ~/.config/fish/config.fish $WDIR/.config/fish/
    # fish functions
    mkdir -p $WDIR/.config/fish/functions
    mkdir -p $WDIR/.config/fish/myfuncs
    cp ~/.config/fish/functions/*.fish $WDIR/.config/fish/functions/
    cp ~/.config/fish/myfuncs/u*.fish $WDIR/.config/fish/myfuncs/
    #remove fish prompt symlink
    rm $WDIR/.config/fish/functions/fish_prompt.fish
fi

# vim
if [ -d ~/.vim ]; then
    echo "Collecting vim configs"
    # clean current dir
    rm -rf $WDIR/.vim
    mkdir -p $WDIR/.vim
    cp ~/.vimrc $WDIR/
    # my preferred vim colours
    cp -r ~/.vim/colors $WDIR/.vim/
fi

# my i3 configurations
if [ -d ~/.config/i3 ]; then
    echo "Collecting i3 configs"
    #clean i3 dir
    rm -rf $WDIR/.config/i3
    mkdir -p $WDIR/.config/i3
    #copy config
    cp -r ~/.config/i3 $WDIR/.config/
fi

# ranger
if [ -d ~/.config/ranger ]; then
    echo "Collecting ranger configs"
    # clean ranger config
    rm -rf $WDIR/.config/ranger
    mkdir -p $WDIR/.config/ranger
    # copy required confs
    cp ~/.config/ranger/{*.py,*.conf,*.sh} $WDIR/.config/ranger/
fi

# compton
echo "Collecting compton config"
cp ~/.config/.compton.conf $WDIR/.config/

# tmux
#echo "Collecting tmux config"
#cp ~/.tmux.conf $WDIR/

# get the wall paper  
#echo "collecting wallpaper"
#cp ~/.config/wall.png $WDIR/.config/
