#!/bin/bash

set -e

if ! [ $(id -u) = 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
CUSER=${SUDO_USER}

echo "Installing neofetch"
add-apt-repository -y -u ppa:dawidd0811/neofetch
apt install neofetch

