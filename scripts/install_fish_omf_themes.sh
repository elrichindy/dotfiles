#!/bin/bash

set -e

if ! [ $(id -u) = 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
CUSER=${SUDO_USER}
CGROUP="$(su $CUSER -c "id -gn")"
echo $CGROUP

# make build dir if not already
su $CUSER -c "mkdir -p $WDIR/build/fish"
cd $WDIR/build/fish

echo "Installing requirments"
apt install -y git curl

# Install fish
if hash fish 2>/dev/null; then 
    echo " fishy fish fish already installed"
else
    echo "Adding fish repositories and required tools"
    apt-add-repository -u -y ppa:fish-shell/release-2
    echo "Installing fish and required tools"
    apt install -y fish
fi

if [ -d ~/.config/fish ]; then
    echo "Changing fish config permissions"
    chown -R $CUSER:"${CGROUP}" ~/.config/fish
fi

#install omf and themes
echo "Installing omf"
su $CUSER -c "curl -L https://get.oh-my.fish > omfinstall"
chmod +x omfinstall
su $CUSER -c "./omfinstall --yes --noninteractive"

if [ -d ~/.config/omf ]; then
    echo "Changing omf config permissions"
    chown -R $CUSER:"${CGROUP}" ~/.config/omf
fi

echo "Installing sushi"
su $CUSER -c "fish -c 'omf install sushi'"

#copy config
echo "Copying fish configs"
su $CUSER -c "cp -r $WDIR/.config/fish ~/.config/"

#echo "Copying omf config"
#su $CUSER -c "cp -r $WDIR/.config/omf ~/.config/"

#set default shell
echo "setting fish as default shell"
chsh -s $(which fish) $CUSER

# clean up
echo "Cleaning up build dir"
rm -rf $WDIR/build/fish

