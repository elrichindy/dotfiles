#!/bin/bash

set -e
set -x
if ! [ $(id -u) = 0 ]; then
    echo "This script must be run as root" 
    exit 1
fi

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
CUSER=${SUDO_USER}
echo "Creating build directories"
su $CUSER -c "mkdir -p $WDIR/build && cd $WDIR/build"

echo "Adding i3 repositories"

echo "adding ubuntu workaround for bug 1522675"
mkdir -p $WDIR/build/keyring
chown _apt $WDIR/build/keyring 
#install from repos as per the instructions here https://i3wm.org/docs/repositories.html
/usr/lib/apt/apt-helper download-file http://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2017.01.02_all.deb $WDIR/build/keyring/keyring.deb SHA256:4c3c6685b1181d83efe3a479c5ae38a2a44e23add55e16a328b8c8560bf05e5f
dpkg -i $WDIR/build/keyring/keyring.deb
echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" >> /etc/apt/sources.list.d/sur5r-i3.list

echo "Installing i3"
apt update && apt install -y i3

# install ranger 
echo "Installing Ranger from source"
su $CUSER -c "cd $WDIR/build && git clone https://github.com/ranger/ranger.git"
cd $WDIR/build/ranger && make install


echo "Installing compton urxvt and other required  tools"
apt install -y compton rxvt-unicode-256color feh 

# copy configs
# urxvt
echo "Copying urxvt configs"
# use @ for sed as $HOME is a path
su $CUSER -c "sed -e s@HOME@$HOME@g $WDIR/.Xresources > ~/.Xresources"
su $CUSER -c "cp -r $WDIR/.config/urxvt ~/.config/"

# compton
echo "Copying compton configs"
su $CUSER -c "cp $WDIR/.config/.compton.conf ~/.config/"
# i3
echo "Copying i3 and i3 status configs"
su $CUSER -c "cp -r $WDIR/.config/i3 ~/.config/" 
su $CUSER -c "cp -r $WDIR/.config/i3status ~/.config/" 

# ranger
echo "Copying ranger configs"
su $CUSER -c "cp -r $WDIR/.config/ranger ~/.config/"

# copy wall paper
echo "Copying wallpaper"
su $CUSER -c "cp $WDIR/.config/wall.png ~/.config/"

# clean up
echo "Removing temp files"
rm -rf $WDIR/build/{keyring,ranger}

echo "Should restart to start using i3, or at least log off and log on"
